#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <linmath.h>

#define uint unsigned int
#define ArrCount(a) (sizeof(a) / sizeof(a[0]))
#define Err(msg) do{fprintf(stderr, msg);}while(0)
#define Chk(e, msg) do{if(!e){Err(msg);goto error;}}while(0);
#define Chq(e) do{if(!e){goto error;}}while(0);

#define WIDTH 1280
#define HEIGHT 720

#define bool  int
#define true  1
#define false 0

#define u64 uint64_t
#define u32 uint32_t
#define u16 uint16_t
#define u8  uint8_t

#define i64 int64_t
#define i32 int32_t
#define i16 int16_t
#define i8  int8_t

#define uint unsigned int
#define usize size_t

#define RADIANS(Dg) (Dg*0.0174533f)
#define BLOCK_COUNT 128

// ===========================
// NOISE FUNCTIONS BY 'nowl'
// https://gist.github.com/nowl/828013
static int SEED = 0;

static int hash[] = {208,34,231,213,32,248,233,56,161,78,24,140,71,48,140,254,245,255,247,247,40,
                     185,248,251,245,28,124,204,204,76,36,1,107,28,234,163,202,224,245,128,167,204,
                     9,92,217,54,239,174,173,102,193,189,190,121,100,108,167,44,43,77,180,204,8,81,
                     70,223,11,38,24,254,210,210,177,32,81,195,243,125,8,169,112,32,97,53,195,13,
                     203,9,47,104,125,117,114,124,165,203,181,235,193,206,70,180,174,0,167,181,41,
                     164,30,116,127,198,245,146,87,224,149,206,57,4,192,210,65,210,129,240,178,105,
                     228,108,245,148,140,40,35,195,38,58,65,207,215,253,65,85,208,76,62,3,237,55,89,
                     232,50,217,64,244,157,199,121,252,90,17,212,203,149,152,140,187,234,177,73,174,
                     193,100,192,143,97,53,145,135,19,103,13,90,135,151,199,91,239,247,33,39,145,
                     101,120,99,3,186,86,99,41,237,203,111,79,220,135,158,42,30,154,120,67,87,167,
                     135,176,183,191,253,115,184,21,233,58,129,233,142,39,128,211,118,137,139,255,
                     114,20,218,113,154,27,127,246,250,1,8,198,250,209,92,222,173,21,88,102,219};

int noise2(int x, int y)
{
    int tmp = hash[(y + SEED) % 256];
    return hash[(tmp + x) % 256];
}

float lin_inter(float x, float y, float s)
{
    return x + s * (y-x);
}

float smooth_inter(float x, float y, float s)
{
    return lin_inter(x, y, s * s * (3-2*s));
}

float noise2d(float x, float y)
{
    int x_int = x;
    int y_int = y;
    float x_frac = x - x_int;
    float y_frac = y - y_int;
    int s = noise2(x_int, y_int);
    int t = noise2(x_int+1, y_int);
    int u = noise2(x_int, y_int+1);
    int v = noise2(x_int+1, y_int+1);
    float low = smooth_inter(s, t, x_frac);
    float high = smooth_inter(u, v, x_frac);
    return smooth_inter(low, high, y_frac);
}

float perlin2d(float x, float y, float freq, int depth)
{
    float xa = x*freq;
    float ya = y*freq;
    float amp = 1.0;
    float fin = 0;
    float div = 0.0;

    int i;
    for(i=0; i<depth; i++)
    {
        div += 256 * amp;
        fin += noise2d(xa, ya) * amp;
        amp /= 2;
        xa *= 2;
        ya *= 2;
    }

    return fin/div;
}
//===============================

const char* const VertShader = 
"#version 330 core\n"
"uniform mat4 projection;\n"
"uniform mat4 view;\n"
"uniform mat4 model;\n"
"layout (location = 0) in vec3 position;\n"
"out float Y;\n"
"void main() {\n"
"gl_Position = projection * view * model * vec4(position, 1.0);\n"
"Y = position.y;\n"
"}\n";

const char* const FragShader = 
"#version 150\n"
"out vec4 Cr;\n"
"in float Y;\n"
"void main() {\n"
"Cr = 0.2 + smoothstep(-1.0, 1.0, Y) * vec4(0.0, 0.7, 0.0, 1.0);\n"
"}\n";


static float ViewX = -50.0f;
static float ViewY = -15.0f;
static float ViewZ = -70.0f;

static bool Keys[1024];
static void 
KeyboardInput(GLFWwindow* Window, int Key, int Scancode, int Action, int Mode)
{
    if (Action == GLFW_PRESS)
    {
        Keys[Key] = true;
    }

    if (Action == GLFW_RELEASE)
    {
        Keys[Key] = false;
    }
}

static uint
BuildShader(const char* VsSource, const char* FsSource)
{
    // TODO Error checking
    uint Program = 0;
    uint FragmentShader = 0;
    uint VertexShader = 0;
    int CompileSuccessful = false;

    VertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(VertexShader, 1, &VsSource, NULL);
    glCompileShader(VertexShader);
    glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &CompileSuccessful);
    Chk(CompileSuccessful, "Failed to compile vertex DrawShader\n");
    
    CompileSuccessful = false;
    FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(FragmentShader, 1, &FsSource, NULL);
    glCompileShader(FragmentShader);
    glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &CompileSuccessful);
    Chk(CompileSuccessful, "Failed to compile fragment DrawShader\n");
    
    CompileSuccessful = false;
    Program = glCreateProgram();
    glAttachShader(Program, VertexShader);
    glAttachShader(Program, FragmentShader);
    glLinkProgram(Program);
    glGetProgramiv(Program, GL_LINK_STATUS, &CompileSuccessful);
    Chk(CompileSuccessful, "Failed to link DrawShader program\n");

    glDeleteShader(VertexShader);
    glDeleteShader(FragmentShader);

    return Program;

error:
    if (VertexShader) glDeleteShader(VertexShader);
    if (FragmentShader) glDeleteShader(FragmentShader);
    if (Program) glDeleteProgram(Program);

    return 0;
}


static uint ShaderProgram;
static uint VBO;
static uint VAO;
static int modelLoc;
static int viewLoc;
static float BlockYData[BLOCK_COUNT][BLOCK_COUNT];
static int projLoc;
static mat4x4 View;
static mat4x4 Proj;

static void
DrawCube(float X, float Y, float Z)
{
    mat4x4 Model;
    mat4x4_translate(Model, X, Y, Z);
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, *Model);

    // Draw container
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);
}

static void
BuildChunk(void)
{
    for (int X = 0; X < BLOCK_COUNT; ++X)
    {
        for (int Z = 0; Z < BLOCK_COUNT; ++Z)
        {
            float Nx = X / BLOCK_COUNT - 0.5;
            float Ny = Z / BLOCK_COUNT - 0.5;
            BlockYData[X][Z] = 10.0 * (perlin2d(X, Z, 0.1, 1.56));
        }
    }
}

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    BuildChunk();
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Terrain Cubes", NULL, NULL);
    glfwMakeContextCurrent(window);

    gladLoadGL();

    glViewport(0, 0, WIDTH, HEIGHT);

    glEnable(GL_DEPTH_TEST);
    glfwSetKeyCallback(window, KeyboardInput);

    ShaderProgram = BuildShader(VertShader, FragShader);

    GLfloat vertices[] = {

        // X     Y     Z    
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,

        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,

         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,

        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f
    };

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0); 

    mat4x4 View;
    mat4x4 Projection;
    mat4x4_translate(View, 0.0f, 1.0f, -10.0f);
    mat4x4_perspective(Projection, RADIANS(45.0f), (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1, 100.0f);
    
    modelLoc = glGetUniformLocation(ShaderProgram, "model");
    viewLoc = glGetUniformLocation(ShaderProgram, "view");
    projLoc = glGetUniformLocation(ShaderProgram, "projection");

    // Game loop
    glUseProgram(ShaderProgram);

    glUniformMatrix4fv(projLoc, 1, GL_FALSE, *Projection);
    while (!glfwWindowShouldClose(window))
    {
        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (Keys[GLFW_KEY_W])
        {
            ViewZ += 1.0f;
        }

        if (Keys[GLFW_KEY_A])
        {
            ViewX += 1.0f;
        }

        if (Keys[GLFW_KEY_S])
        {
            ViewZ -= 1.0f;
        }

        if (Keys[GLFW_KEY_D])
        {
            ViewX -= 1.0f;
        }

        if (Keys[GLFW_KEY_F])
        {
            ViewY += 1.0f;
        }

        if (Keys[GLFW_KEY_R])
        {
            ViewY -= 1.0f;
        }


        mat4x4_translate(View, ViewX, ViewY, ViewZ);
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, *View);
        for (int X = 0; X < BLOCK_COUNT; ++X)
        {
            for (int Z = 0; Z < BLOCK_COUNT; ++Z)
            {
                for (int Y = BlockYData[X][Z] - 4; Y < BlockYData[X][Z]; ++Y)
                {
                    DrawCube(X, Y, Z);
                }
            }
        }
        glfwSwapBuffers(window);
    }
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glfwTerminate();
    return 0;
}

