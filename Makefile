Executable := tg.exe
Sources := tg.c
Libraries := "glad.lib" "glfw3.lib" "opengl32.lib" "kernel32.lib" "user32.lib" "gdi32.lib" "winspool.lib" "comdlg32.lib" "advapi32.lib" "shell32.lib" "ole32.lib" "oleaut32.lib" "uuid.lib" "odbc32.lib" "odbccp32.lib"

LinkerFlags := -incremental:no -entry:mainCRTStartup -libpath:libs -machine:x64 $(Libraries) -nodefaultlib:libcmt 
CompilerFlags := -nologo -MP -TC -GR- -Gm- -EHa- -Iinclude -Fe$(Executable) -Fo./build/
DebugFlags := -MDd -Z7 -Od 
ReleaseFlags := -MT -Oi -Os


.PHONY: clean debug release run tags

all: debug

tags:
	@ctags -R code

clean:
	@rm -f build/* $(Executable)

debug: tags
	@cl $(CompilerFlags) $(DebugFlags) $(Sources) -link $(LinkerFlags) -nodefaultlib:msvcrt

release:
	@cl $(CompilerFlags) $(ReleaseFlags) $(Sources) -link $(LinkerFlags) -subsystem:windows

run: debug
	@./$(Executable)
